# README #

Welcome to Hummingbird3: GGM eCommerce [Extension]

* To install, simply download the package and place into your HB3 installation. You must place this within the workbench directory with the following namespace "tickbox".
* Visit your CMS > Plugins
* "Activate" Plugin.

You will need to install the Base eCommerce system plugin first.

### What is this repository for? ###

* To provide additional and non-standardised functionality to the base eCommerce system.

### Who do I talk to? ###

* Project Owner: Daryl Phillips <darylp@tickboxmarketing.co.uk>
* Support Team: Support @ Tickbox Marketing <support@tickboxmarketing.co.uk>