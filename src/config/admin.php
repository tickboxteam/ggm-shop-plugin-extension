<?php

return array(
    /*
    |--------------------------------------------------------------------------
    | Navigation items
    |--------------------------------------------------------------------------
    |
    */
    'navigation_items' => [
        'ggm-royal-mail' => [
            'label' => 'Royal Mail Exports',
            'url'   => url('hummingbird/shop-ggm/royal-mail-download'),
            'order' => 100,
            'parent' => 'tickbox-shop.children.tickbox-shop-transactions',
            'permissions' => [
                'tickbox.shop.transactions.read'
            ]
        ]
    ],


    /*
    |--------------------------------------------------------------------------
    | Navigation Groups
    |--------------------------------------------------------------------------
    |
    */
    'navigation_groups' => []
);
