<?php namespace GGM\Shop;

use Hummingbird\PluginServiceProvider;
use Hummingbird\Interfaces\PluginServiceProviderInterface;

class ShopServiceProvider extends PluginServiceProvider implements PluginServiceProviderInterface {
    protected $files        = [
        __DIR__ . '/routes.php'
    ];


    /**
     * Set the plugin name
     *
     * @return void
     */
    public function setPluginName() {
        $this->pluginName = 'plugins.GGM.Shop';
    }


    /**
     * Set the package name
     *
     * @return void
     */
    public function setPackageName() {
        $this->packageName = "ggm/ggm-shop";
    }


    /**
     * Set the directory for our views
     *
     * @return void
     */
    public function setViewDirectory() {
        $this->views = __DIR__ . '/views';
    }


    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->package("ggm/ggm-shop", null, realpath(__DIR__));

        $this->app[$this->pluginName]->boot();
    }


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        parent::register();
    }


    /**
     * Registering the main Plugin file used for Plugin Services
     *
     * @return void
     */
    public function registerPlugin() {
        $this->app[$this->pluginName] = $this->app->share(function($app) {
            return new Plugin($this->PluginManager->getPackageInformation('GGM', 'GgmShop'));
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }
}
