<?php namespace GGM\Shop\Controllers\Admin;

use ArrayHelper;
use Carbon\Carbon;
use CmsbaseController;
use General;
use Input;
use Redirect;
use SplTempFileObject;
use Validator;
use View;

use League\Csv\Writer;

use Tickbox\Shop\Models\ShopOrder;

/**
 * 
 */
class RoyalMailExportsController extends CmsbaseController {

    public function __construct() {
        parent::__construct();

        /* Register breadcrumb */
        $this->data['breadcrumbs'][] = array(
            'classes' => '',
            'icon' => '',
            'title' => 'Shop',
            'url' => '/' . General::backend_url() . '/shop/'
        );

        /* Register breadcrumb */
        $this->data['breadcrumbs'][] = array(
            'classes' => '',
            'icon' => '',
            'title' => 'Products',
            'url' => '/' . General::backend_url() . '/shop/products/'
        );
    }

    /**
     * Show the base export page
     */
    public function index() {
        $this->data['start_date']  = (Input::old('start_date')) ? Carbon::createFromFormat('d/m/Y', Input::old('start_date')) : Carbon::now()->subMonth(1);
        $this->data['finish_date'] = (Input::old('finish_date')) ? Carbon::createFromFormat('d/m/Y', Input::old('finish_date')) : Carbon::now();

        return View::make('ggm/ggm-shop::admin.royal-mail.index', $this->data);
    }

    /**
     * Run the exports
     */
    public function export() {
        $Validator = Validator::make(Input::all(), ['start_date' => 'required|date_format:d/m/Y|before:tomorrow', 'finish_date' => 'required|date_format:d/m/Y|after:' . Carbon::createFromFormat('d/m/Y', Input::get('start_date'))->subDay()->toDateString()]);

        if( $Validator->fails() ) {
            return Redirect::back()->withInput()->with('error', 'Please make sure you input the correct start/end dates');
        }

        // Get orders
        $ShopOrders = ShopOrder::where('status', '=', 'PROCESSING')->where(function($query) {
            $query->where('payment_date', '>=', Carbon::createFromFormat('d/m/Y', Input::get('start_date'))->format('Y-m-d 00:00:00'))
                ->where('payment_date', '<=', Carbon::createFromFormat('d/m/Y', Input::get('finish_date'))->format('Y-m-d 23:59:59'));
        })->orderBy('id', 'DESC')->get();

        if( $ShopOrders->count() <= 0 ) {
            return Redirect::route('hummingbird.shop-ggm.royal-mail-download.index')->with('error', 'Nothing to export.');
        }

        $writer = Writer::createFromFileObject(new SplTempFileObject());
        $writer->insertOne(array("G+GM order number","Date of order","Customer name","1st Line Address","2nd Line Address","City","County","Postcode","Country","Number of items in shipment","Net weight","Service Code","Safe Place","Email","Email notification","Tel","Format"));

        foreach($ShopOrders as $ShopOrder) {
            $ShopOrder_del_address = ArrayHelper::cleanExplodedArray( [ $ShopOrder->del_address1, $ShopOrder->del_address2, $ShopOrder->del_town, $ShopOrder->getDelCountyAttribute() ] );

            $ShopOrder_Weight = 0;

            foreach( $ShopOrder->shop_items as $ShopItem ) {
                $ShopOrder_Weight += $this->calculateShopWeight($ShopItem);
            }
            $ShopOrder_Weight = ($ShopOrder_Weight > 0) ? $ShopOrder_Weight/1000 : 0;

            $writer->insertOne(array(
                $ShopOrder->order_id,
                $ShopOrder->payment_date,
                $ShopOrder->del_name,
                $ShopOrder->del_address1, 
                $ShopOrder->del_address2,
                $ShopOrder->del_town,
                $ShopOrder->getDelCountyAttribute(),
                $ShopOrder->del_postcode,
                $ShopOrder->delivery_country()->first()->country,
                "1",
                $ShopOrder_Weight,
                ($ShopOrder->delivery_country()->first()->country != "UNITED KINGDOM") ? 'OTA' : 'TPS48',
                substr(str_replace("\r\n", "", $ShopOrder->delivery_notes), 0, 90),
                $ShopOrder->inv_email,
                (!empty($ShopOrder->inv_email)) ? 'Y' : '',
                "=\"" . $ShopOrder->inv_telephone . "\"",
                "Parcel"
            ));
        }  

        $writer->output("royal-mail-postage.csv");
    }

    /**
     * Calculate the weight of this product.
     * @param  OrderItem $ShopItem [description]
     * @return Integer
     */
    protected function calculateShopWeight($ShopItem) {
        if( $ShopItem->item ) {
            if( $ShopItem->item->unit_weight > 0 ) {
                return $ShopItem->item->unit_weight;
            }

            if( !$ShopItem->item->is_master ) {
                if( $ShopItem->item->product->master_variant()->first()->unit_weight > 0 ) {
                    return $ShopItem->item->product->master_variant()->first()->unit_weight;
                }
            }
        }

        return 0;
    }
}