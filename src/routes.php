<?php 
Route::group(array('before' => 'install.before'), function() {
    /*
     * Administrator pages
     */
    Route::group(array('prefix' => General::backend_url(), 'before' => 'auth.check'),  function() {
        Route::post('shop-ggm/royal-mail-download/export', array('as' => 'hummingbird.shop.royal-mail-export', 'uses' => 'GGM\Shop\Controllers\Admin\RoyalMailExportsController@export'));
                
        Route::resource('shop-ggm/royal-mail-download', 'GGM\Shop\Controllers\Admin\RoyalMailExportsController', array('only' => ['index']));
    });
});