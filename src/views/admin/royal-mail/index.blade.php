@extends('HummingbirdBase::cms.layout')

@section('styles')
    <link rel="stylesheet" href="/themes/hummingbird/default/lib/datetimepicker/datetimepicker.css" />
@stop

@section('breadcrumbs')
    @if (count($breadcrumbs) > 0)
        <div class="row">
            <div class="col-md-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <?php $i = 0;?>
                    @foreach ($breadcrumbs as $breadcrumb)
                        <?php $i++;?>
                        <li>
                        @if($breadcrumb['url'] != $_SERVER["REQUEST_URI"] AND $breadcrumb['url'] != '')
                            <a href="{{ $breadcrumb['url'] }}">@if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}</a>
                        @else
                            @if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}
                        @endif
                        </li>
                    @endforeach
                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>
    @endif
@stop

@section('content')
    @if (Session::has('success'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-success fade in">
                    {{ Session::get('success') }}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('message'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-info fade in">
                    {{ Session::get('message') }}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('error'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-danger fade in">
                    {{ Session::get('error') }}
                </div>
            </div>
        </div>  
    @endif

    @if(isset($errors))
        <div class="row">
            @foreach ($errors->all() as $error)
                <div class="col-md-12 text-center">
                    <div class="alert alert-block alert-danger fade in">
                        {{ $error }}
                    </div>
                </div>
            @endforeach
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <section class="panel" style="background-color:white;padding:20px;">
                <h1>Royal Mail Postage Calculations</h1>
                <p>Search for orders and download in <strong>.csv</strong> format to import into Royal Mail. Change the search dates and click export. The CSV will automatically generate and download.</p>
                
                <hr />
                
                {{ Form::open(array('route' => 'hummingbird.shop.royal-mail-export'), array('class' => 'form-horizontal')) }}
                    <div class="row">
                        <div class="col-sm-5">
                            Will export between the following dates <strong>{{ $start_date->format('j M, Y') }}</strong> and <strong>{{ $finish_date->format('j M, Y') }}</strong>.
                        </div>

                        <div class="col-sm-6 col-sm-offset-1">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                {{ Form::text('start_date', $start_date->format('d/m/Y'), array('class' => 'form-control datetimepicker datetimepicker-start_date')) }}

                                <span class="input-group-addon" style="border-left:0;border-right:0;">- to - </span>
                                
                                {{ Form::text('finish_date', $finish_date->format('d/m/Y'), array('class' => 'form-control datetimepicker datetimepicker-finish_date')) }}
                                
                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-primary rounded"><i class="fa fa-download"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                {{ Form::close() }}
            </section>
        </div>
    </div>
@stop

@section('scripts')
    <script src="/themes/hummingbird/default/lib/moment/moment.js"></script>
    <script src="/themes/hummingbird/default/lib/datetimepicker/datetimepicker.js"></script>

    <script>
        $(document).ready(function() {
            if($('.datetimepicker').length > 0) {
                $('.datetimepicker-start_date').datetimepicker( {
                    icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    },
                    defaultDate: $(this).val(),
                    maxDate: new Date(),
                    format: "DD/MM/YYYY"
                });

                $('.datetimepicker-finish_date').datetimepicker( {
                    icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    },
                    defaultDate: $(this).val(),
                    format: "DD/MM/YYYY",
                    maxDate: new Date()
                });

                if($(".datetimepicker-start_date").length > 0) {
                    $(".datetimepicker-start_date").on("dp.change", function (e) {
                        $('.datetimepicker-finish_date').data("DateTimePicker").minDate(e.date);
                    });
                }

                if($(".datetimepicker-finish_date").length > 0) {
                    $(".datetimepicker-finish_date").on("dp.change", function (e) {
                        $('.datetimepicker-start_date').data("DateTimePicker").maxDate(e.date);
                    });
                }                 
            }
        });
    </script>
@stop