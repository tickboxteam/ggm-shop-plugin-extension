<?php namespace GGM\Shop;

use Hummingbird\Classes\PluginBase;

/**
 * Plugin Installer to handle the migrations and seeding
 * Plugin specific information
 *
 * @package hummingbird
 * @author Daryl Phillips
 *
 */

class Plugin extends PluginBase {
	public function __construct($package_details = array()) {
		$this->package 	= $package_details;
	}

    /**
     * Return the package name (if available)
     * 
     * @return String
     */
    public function getPackageName() {
        return "ggm-shop";
    }
}