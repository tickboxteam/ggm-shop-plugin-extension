# GGM: eCommerce System Extension
Providing additional and non-standardised functionality to the base eCommerce system.

# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.4] - 2020-11-04
### Updated
- Default export values for additional email column (for Royal Mail)


## [1.0.3] - 2020-10-28
### Added
- New field for email notifications added to the Royal mail export


## [1.0.2] - 2020-02-13
### Updated
- Updated the Service Codes for non-UK addresses


## [1.0.1] - 2020-02-04
### Added
- New export fields (Address fields and Format)

### Updated
- Existing export fields and calculating weight
- Delivery notes in export limited to 90 characters (if set)

### Fixed
- Export issues with telephone/emails not appearing


## [1.0.0] - 2019-09-25
### Added
- New plugin to allow for Royal Mail Exports to be produced on request